import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpEventType } from '@angular/common/http';
import * as XLSX from 'xlsx';
import * as FileSaver from 'file-saver';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})

export class FileUploadComponent implements OnInit {
  fileData: File = null;
  fileDataArr: any;
  previewUrl: any = null;
  fileUploadProgress: string = null;
  uploadedFilePath: string = null;
  errMessege: boolean = false;
  errMessegeFileType: boolean = false;
  tablArr: Array<{ File_Id: Number, File_Name: String, User: String, Status: String, ngClassErr: boolean, Data: any }> = [];
  arrayBuffer: any;
  valid_excel: any;
  ngClassErr: boolean = false;

  constructor(private http: HttpClient) {
    this.tablArr = JSON.parse(localStorage.getItem('services_assigned') || '[]');
  }

  ngOnInit(): void {
  }

  async fileProgress(fileInput: any) {
    this.fileData = <File>fileInput.target.files;
    this.fileDataArr = this.fileData;
    for (let index = 0; index < this.fileDataArr.length; index++) {
      const file = this.fileDataArr[index];
      let fileSize = file.size / 1024 / 1024; //MB convert
      if (fileSize <= 1) {
        if (file.type === "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
          this.valid_excel = await this.excel_validation(file);
          let objnm = Object.keys(this.valid_excel)[0];
          this.ngClassErr = this.valid_excel[objnm].length === 0 ? false : true;
          let mimeType = file.type;
          if (mimeType.match(/image\/*/) == null) { console.log("Image Null"); }
          let reader = new FileReader();
          reader.readAsDataURL(file);
          reader.onload = (_event) => { this.previewUrl = reader.result; }
          this.tablArr.push({ File_Id: 1, File_Name: file.name, User: 'Arnab Mandal', Status: 'Uploaded', ngClassErr: this.ngClassErr, Data: this.valid_excel });
          localStorage.setItem('services_assigned', JSON.stringify(this.tablArr));
        } else {
          this.errMessegeFileType = true;
        }
      } else {
        this.errMessege = true;
      }
    }
  }

  async excel_validation(files: any) {
    return new Promise(resolve => {
      const reader = new FileReader();
      reader.onload = async (event) => {
        const data = reader.result;
        let workBook = XLSX.read(data, { type: 'binary' });
        let jsonData = await workBook.SheetNames.reduce((initial: { [x: string]: unknown[]; }, name: string | number) => {
          const sheet = workBook.Sheets[name];
          initial[name] = XLSX.utils.sheet_to_json(sheet);
          return initial;
        }, {});
        const dataString = jsonData;
        resolve(dataString);
      }
      reader.readAsBinaryString(files);
    });
  }

  setDownload(data?: any) {
    const blob = new Blob([JSON.stringify(data)], { type: 'application/vnd.ms-excel;charset=utf-8' });
    FileSaver.saveAs(blob, "XLS_File" + new Date().getTime() + '.xlsx');
  }

  onSubmit() {
    const formData = new FormData();
    formData.append('file', this.fileData);
    this.http.post('url/to/your/api', formData)
      .subscribe(res => {
        console.log(res);
        this.uploadedFilePath = 'res';
        alert('SUCCESS !!');
      })
  }

}
