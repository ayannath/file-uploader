import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormBuilder, FormGroup } from  '@angular/forms';
import { UploadService } from  './upload.service';

import { FileUploadComponent } from './file-upload/file-upload.component';
import { FileProcessComponent } from './file-process/file-process.component';


const appRoutes: Routes = [
  { path: '', component: FileUploadComponent },
  { path: 'file-process', component: FileProcessComponent },

  { path: '',
    redirectTo: '/app-file-upload',
    pathMatch: 'full'
  },
  { path: 'file-process',
    redirectTo: '/app-file-process',
    pathMatch: 'full'
  },
  // { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
